import About from '../routes/About.svelte'
import Services from '../routes/Services.svelte'
import Contact from '../routes/Contact.svelte'

export default [
   {
      path: '/',
      name: 'About',
      component: About
   },
   {
      path: '/about',
      name: 'About',
      component: About
   },
   {
      path: '/services',
      name: 'Services',
      component: Services
   },
   {
      path: '/contact',
      name: 'Contact',
      component: Contact
   },
]
