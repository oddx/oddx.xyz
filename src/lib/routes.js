export default [
	{
		path: '/about',
		name: 'About'
	},
	{
		path: '/services',
		name: 'Services'
	},
	{
		path: '/contact',
		name: 'Contact'
	}
]
